<?php

namespace Drupal\multiple_select\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * BulkConfigForm form.
 */
class BulkConfigForm extends FormBase {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->state = $container->get('state');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'multiple_select_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $data = $this->state->get('multiple_select_config');
    if (!is_null($data)) {
      $shared_bulk_config = json_decode($data, TRUE);
    }
    $options = [
      'node' => [
        'readable' => 'Node',
        'type' => 'node_type',
      ],
      'media' => [
        'readable' => 'Media',
        'type' => 'media_type',
      ],
    ];

    foreach ($options as $type => $entity) {
      $entity_types = $this->getTypes($entity['type']);
      if (count($entity_types) > 0) {

        $form[$type] = [
          '#type' => 'label',
          '#title' => $entity['readable'],
          '#prefix' => '<br/>',
        ];
      }

      foreach ($entity_types as $entity_type) {
        $entity_fields = [];
        $bundle_fields = $this->entityFieldManager->getFieldDefinitions($type, $entity_type->id());
        foreach ($bundle_fields as $field) {
          if ($field->getType() == 'list_string' || $field->getType() == 'entity_reference') {
            $entity_fields[$field->getName()] = $field->getLabel();
          }
        }

        $default = !is_null($data) && isset($shared_bulk_config[$entity_type->id()]) ? 1 : 0;
        $form[$entity_type->id()] = [
          '#type' => 'checkbox',
          '#title' => $entity_type->label(),
          '#default_value' => $default,
        ];

        if (empty($entity_fields)) {
          $form[$entity_type->id()]['#disabled'] = TRUE;
        }
        else {
          $default_fields = !is_null($data) && isset($shared_bulk_config[$entity_type->id()]) ? $shared_bulk_config[$entity_type->id()] : NULL;
          $form[$entity_type->id() . '_' . $type] = [
            '#type' => 'select',
            '#multiple' => TRUE,
            '#default_value' => $default_fields,
            '#options' => $entity_fields,
            '#states' => [
              'visible' => [
                ':input[name="' . $entity_type->id() . '"]' => ['checked' => TRUE],
              ],
            ],
          ];
        }

      }
    }
    $form['text'] = [
      '#markup' => $this->t('NOTE: only field of type "Check boxes" will be affected.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('multiple_select_config', NULL);
    $options = [
      'node' => [
        'readable' => 'Node',
        'type' => 'node_type',
      ],
      'media' => [
        'readable' => 'Media',
        'type' => 'media_type',
      ],
    ];
    foreach ($options as $type => $entity) {
      foreach ($this->getTypes($entity['type']) as $entity_type) {
        if ($form_state->getValue($entity_type->id()) != 0) {
          $shared_bulk_config[$entity_type->id()] = $form_state->getValue($entity_type->id() . '_' . $type);
        }
      }
    }
    if (isset($shared_bulk_config)) {
      $this->state->set('multiple_select_config', json_encode($shared_bulk_config));
    }
    $this->messenger()->addStatus($this->t('Configurations successfully saved.'));
  }

  /**
   * Get existing entity types.
   */
  public function getTypes($type) {
    $types = [];
    if ($this->entityTypeManager->hasDefinition($type)) {
      foreach ($this->entityTypeManager->getStorage($type)->loadMultiple() as $instance) {
        $types[] = $instance;
      }
    }
    return $types;
  }

}
