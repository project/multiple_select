<?php

namespace Drupal\Tests\multiple_select\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test module.
 *
 * @group multiple_select
 */
class CrudFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'multiple_select',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'seven';

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'access multiple select config page',
      'access content',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/content/multiple-config');
    $this->assertText('Multiple Select Helper');
  }

}
